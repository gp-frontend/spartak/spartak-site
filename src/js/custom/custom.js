/*----------------------------------------
 INIT AOS ANIMATE
 ----------------------------------------*/

AOS.init({
  duration: 1000,
  disable: 'mobile',
  once: false,
  anchorPlacement: 'top-bottom'
});

/*----------------------------------------
 TRANSITION SCROLL
 ----------------------------------------*/
$('.scroll').on('click', function (e) {
  // e.preventDefault();
  var anchor = $(this)
  $('html, body').stop().animate({
    scrollTop: $(anchor.attr('href')).offset().top
  }, 1000)
})
/*----------------------------------------
  PHONE MASK
----------------------------------------*/
$(".js-mask-number").mask("0#");
$(".js-mask-phone").mask("+7 (000) 000-00-00");
$(".js-mask-time").mask("00 : 00");
$(".js-mask-date").mask("00.00.0000");

/*----------------------------------------
  SELECTIZE INIT
----------------------------------------*/

$('.sumo-select').SumoSelect();

/*----------------------------------------
  MENU
----------------------------------------*/

var buttonOpenMenu = $('.js-menu-open'),
    buttonOpenClose = $('.js-menu-close'),
    menu = $('.js-menu');

$('<div>', { class: 'menu-ovelay'}).appendTo('.header');

function openMenu (e) {
  e.preventDefault();

  menu.addClass('menu_open');
  $('.menu-ovelay').addClass('menu-ovelay_visible');
  $('body').addClass('menu-open');
}

function closeMenu (e) {
  e.preventDefault();

  menu.removeClass('menu_open');
  $('.menu-ovelay').removeClass('menu-ovelay_visible');
  $('body').removeClass('menu-open');
}

buttonOpenMenu.click(openMenu);
buttonOpenClose.click(closeMenu);
$('body .menu-ovelay').click(closeMenu);


/*----------------------------------------
  HEADER PANEL
----------------------------------------*/

var headerPanel = $('.js-header-panel'),
    buttonOpenHeaderPanel = $('.js-location'),
    buttonClosenHeaderPanel = $('.js-panel-close');

$('<div>', { class: 'panel-ovelay'}).appendTo('.header');

function openHeaderPanel (e) {
  e.preventDefault();

  buttonOpenHeaderPanel.toggleClass('header__location_active');
  headerPanel.toggleClass('header-panel_open');
  $('.panel-ovelay').toggleClass('panel-ovelay_visible');
  $('body').toggleClass('panel-open');
}

function closeHeaderPanel (e) {
  e.preventDefault();

  buttonOpenHeaderPanel.removeClass('header__location_active');
  headerPanel.removeClass('header-panel_open');
  $('.panel-ovelay').removeClass('panel-ovelay_visible');
  $('body').removeClass('panel-open');
}

buttonOpenHeaderPanel.click(openHeaderPanel);
buttonClosenHeaderPanel.click(openHeaderPanel);
$('body .panel-ovelay').click(openHeaderPanel);


/*----------------------------------------
  SLICK INIT
----------------------------------------*/

var mainNewsCarousel = $('.js-main-news'),
    reviewsCarousel = $('.js-reviews');

mainNewsCarousel.slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  dots: false,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        dots: true
      }
    },
    {
      breakpoint: 601,
      settings: {
        slidesToShow: 1,
        dots: true
      }
    },
  ]
});

reviewsCarousel.slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  dots:true,
  fade: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        fade:false
      }
    },
  ]
});

/*----------------------------------------
  SLICK WITH CUSTOM JS
 ----------------------------------------*/

// slider
var $news = $('.js-news');

settings_slider = {
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: true,
  arrows: false,
  mobileFirst: true,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2
      }
    },
  ]
}

// slick on mobile
function slick_on_mobile(slider, settings){

  $(window).on('load resize', function() {

    if ($(window).width() > 767) {

      if (slider.hasClass('slick-initialized')) {
        slider.slick('unslick');
      }

      return
    }
    if (!slider.hasClass('slick-initialized')) {
      return slider.slick(settings);
    }
  });
};

slick_on_mobile( $news, settings_slider);
