# Верстка сайта "Спартак"


## УСТАНОВКА

* Можно скачать проект с панели [https://gitlab.com/](https://gitlab.com/) или в консоле `git` ввести команду: `clone https://gitlab.com/site-name` ;
* Для запуска и работы с проектом требуется [`node js`](https://nodejs.org/en/) ;
* Чтобы запустить проект нужно установить [`gulp.js`](http://gulpjs.com/) на ваш компьютер глобально ;
* Установить плагины `gulp.js` можно с помощью команды: `npm i -D` ;
* Команда `gulp build` запускает все задачи и собирает проект (без watch, server) ;
* Запуск проекта осуществляется с помощью команды: `gulp` .

## ИНСТРУКЦИЯ

### 1. Gulp

* Сборка проекта верстки осуществляется с помощью [`gulp.js`](http://gulpjs.com/);
* Все файлы исходники находятся в папке `src/`, а продакшн файлы в папке `build/`;
* Команда `gulp build` запускает все задачи и собирает проект (без watch, server);
* Команда `gulp` запускает проект и запускает watch и browser sync.

### 2. Шаблонизатор pug

* Для проекта был использован препроцессор `HTML` - [`PUG`](https://pugjs.org/);
* Файлы препроцессора хранятся в папке `src/pug`, которые компилируется в папку `build/`.

### 3. Стили сайта

* Стили сайта написаны с помощью препроцессора [`SASS`](http://sass-scss.ru/) (SCSS). `Gulp.js` компилирует все файлы `SCSS` в `CSS`, из папки `src/sass` в `build/css`. Так же в папке `build` находится и минифицированный файл `build/css/*.min.css`. Стили уже содержат префиксы для кроссбраузерности ;
* Для адаптивности используется сетка Bootstrap 4;

* Все стили помечены комментариями:

```css
/*----------------------------------------
  BREADCRUMBS
----------------------------------------*/

/* end  стили хлебных крошек */

```

### 3.Скрипты( javascript ):

* Скрипты подключаются перед закрывающимся тегом `<body>`;
* Все по отдельности находятся в папке `src/js/`. Компилируются все файлы `js` в один `app.js` ;
* Проект использует [`jquery`](https://jquery.com/), [`modernizr`](https://modernizr.com/);

### 4. Изображения:

* В папке `build/uploads` хранятся все изображения, которые будут динамически подгружены из админки. Все изображения в этой папке не нужны при сборке проекта в продакшн;
* В папке `build/images` хранятся все изображения, которые являются системными и необходимы для стиливого оформления сайта.

### 5. Критерии и чеклист верстки:

| Критерии и чеклист верстки                           	| Выполнено 	|
|------------------------------------------------------	|:---------:	|
| Проверка валидности HTML кода всех страниц на w3c    	|     √     	|
| Отсутствие ошибок, при проверке w3c валидатором  	|     ☓     	|
| Шрифты подключены кроссбраузерно                     	|     √     	|
| В консоле браузера отсутствуют ошибки                	|     √     	|
| Наличие фавиконок для всех платформ и браузеров      	|     √     	|
| Подключен Normalize.css / Reset.css / Reboot.css     	|     √     	|
| Подключен Jquery                                     	|     √     	|
| Стили для печати заданы и работают корректны.        	|     ☓     	|
| Проверка desktop браузеров - кроссбраузерная верстка 	|     ☓     	|
| Проверка mobile браузеров - кроссбраузерная верстка  	|     ☓     	|
| Верстка Pixel perfect                                	|     x     	|
| Все иконки в svg формате                             	|     √     	|
| Мета теги title и description  на всех страницах     	|     √     	|
| Все стили подключены в head                          	|     √     	|
| Все скрипты подключены после  закрытого тега body    	|     √     	|
| Задан язык страницы   html lang="ru"                	|     √     	|
| Нет ссылок без href                                  	|     √     	|
| Для всех изображений прописан alt                    	|     √     	|
| На каждой странице присутствует h1                   	|     √     	|
| HTML5 формы, валидация                               	|     √     	|
| Отсутствие пустых ссылок , кнопок, блоков            	|     √     	|
| Ресайз textarea не должен ломать вёрстку             	|     √     	|
| Адаптивные изображения(max-width:100%; height:auto;) 	|     √     	|
| Поддержка Retina                                     	|     ☓     	|
| Доступность с клавиатуры с :focus(нет outline:none)  	|     ☓     	|
| Ссылки на чужие сайты должны быть с target="_blank"  	|     √     	|
| Ссылки на внешние ресурсы с target="_blank" должны иметь атрибут rel="noopener", чтобы предотвратить фишинговые атаки типа tab nabbing. Если нужно поддерживать старые версии Firefox, используйте rel="noopener noreferrer".                 	|     ☓     	|
| Уникальные ID                                        	|     √     	|
| Все классы и id, используемые в JS файлах, начинаются с префикса js- и не участвуют в назначении стилей.                                                	|     √     	|
| Использование БЭМ в стилях                           	|     √     	|
| На сайте используется код base64 в стилях            	|     √     	|
