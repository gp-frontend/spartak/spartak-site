'use strict';

// ////////////////////////////////////////////////
//
// GULP СБОРКА ДЛЯ ВЕРСТКИ САЙТОВ
//
// // /////////////////////////////////////////////

/*----------------------------------------
	PLUGINS
----------------------------------------*/

var gulp = require('gulp'),
	// РАБОТАЕМ С ПРОИЗВОДИТЕЛЬНОСТЬЮ СБОРКИ
	gulpLoadPlugins = require('gulp-load-plugins'),
	$ = gulpLoadPlugins(),
	gulpIf = require('gulp-if'),

	//sprite png files
	spritesmith = require('gulp.spritesmith'),

	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	rsync = require('gulp-rsync'),
	plumber = require('gulp-plumber'),
  svgmin = require('gulp-svgmin'),
  gulpsvgtojsontoscss = require('gulp-svg-to-json-to-scss');

/*----------------------------------------
	PATHS
----------------------------------------*/
var paths = require('./gulp/paths');
console.log($);

// ПОДКЛЮЧАЕМ ОТДЕЛЬНО TASKS
function lazyRequireTask(taskName, path, options) {
	options = options || {};
	options.taskName = taskName;
	gulp.task(taskName, function(callback) {
	var task = require(path).call(this, options);

	return task(callback);
	});
}

/*----------------------------------------
	BROWSER SYNC
----------------------------------------*/

gulp.task('server', ['sass', 'pug'], function() {
	browserSync.init({
		server: "./build",
		port: 6722
	});
});

gulp.task('watch-html', ['pug'], function () {
	browserSync.reload();
});

/*----------------------------------------
	HTML
----------------------------------------*/

lazyRequireTask('html', './gulp/tasks/html', {
	src: paths.html.src,
	dest: paths.html.dest
});

/*----------------------------------------
	PUG
----------------------------------------*/
lazyRequireTask('pug', './gulp/tasks/pug', {
	src: paths.pug.src,
	dest: paths.pug.dest
});

/*----------------------------------------
	DATA JSON
----------------------------------------*/
lazyRequireTask('data', './gulp/tasks/data', {
	src: paths.data.src,
	dest: paths.data.dest
});

/*----------------------------------------
	STYLES SASS
----------------------------------------*/

lazyRequireTask('sass', './gulp/tasks/sass', {
	src: paths.sass.app,
	dest: paths.sass.dest
});

lazyRequireTask('sass:ui', './gulp/tasks/sass', {
  src: 'src/sass/ui.scss',
  dest: paths.sass.dest
});

/*----------------------------------------
	JS
----------------------------------------*/

gulp.task('watch-js', ['js'], function () {
	browserSync.reload();
});

lazyRequireTask('js', './gulp/tasks/js', {
	src: paths.js.src,
	dest: paths.js.dest,
	minDest: paths.js.dest
});


/*----------------------------------------
	SVG
----------------------------------------*/

//SVG SPRITE
lazyRequireTask('svg', './gulp/tasks/svg', {
	src: paths.svg.src,
});


//SVG SPRITE BASE 64
lazyRequireTask('svg:base', './gulp/tasks/svgBase', {
	src: paths.base64.src,
	dest: paths.base64.dest
});

/*----------------------------------------
	SVG JSON DATA
----------------------------------------*/

gulp.task('svg:data', function() {

  gulp.src('src/svg/*.svg')
    .pipe(svgmin())

    .pipe(gulpsvgtojsontoscss({
      jsonFile: 'icons.json',
      noExt:true,
      delim:"_"
    }))

    .pipe(gulp.dest('src/data/assets'));
});



/*----------------------------------------
	SPRITE PNG FILES
----------------------------------------*/
gulp.task('sprite', function () {
	var spriteData = gulp.src('src/images/sprite/*')
		.pipe(spritesmith({
			imgName: 'sprite.png',
			// retinaImgName: 'sprite@2x.png',
			cssName: '_sprite-png.scss',
			imgPath:'../images/sprite/sprite.png',
			padding: 15
		})
	);

var imgStream = spriteData.img
	.pipe(gulp.dest('build/images/sprite'));

var cssStream = spriteData.css
	.pipe(gulp.dest('src/sass/sprite/'));

});

gulp.task('jsMinSync', function () {
	return gulp.src('src/js/minifier/**/*.js')
		.pipe(plumber())
		.pipe(gulp.dest('build/js/'))
		.pipe(browserSync.stream());
});

/*----------------------------------------
	MINIFY IMAGES
----------------------------------------*/

lazyRequireTask('image:min', './gulp/tasks/images', {
	src: paths.images.src,
	dest: paths.images.dest
});

lazyRequireTask('uploads:min', './gulp/tasks/images', {
	src: paths.images.srcUploads,
	dest: paths.images.destUploads
});

gulp.task('copy', function () {
	return gulp.src('src/assets/**/*')
		.pipe(gulp.dest('build/'))
});


gulp.task('watch', function () {
		// data
	gulp.watch('src/data/**/*.json', ['data']);
		// sass
	gulp.watch('src/sass/**/*.scss', ['sass', 'sass:ui']);
	// html
	// gulp.watch(['src/html/**/*.html'], ['watch-html']);
	gulp.watch(['src/pug/**/*.{pug, html}'], ['watch-html']);
	// js
	gulp.watch("src/js/**/*.js", ['watch-js']);
	// images
	gulp.watch("src/images/*", ['image:min']);
	gulp.watch("src/uploads/*", ['uploads:min']);
	// svg
	gulp.watch('src/svg/*.svg', ['svg']).on('change', function(event){
		if (event.type === 'deleted') {
			$.remember.forget('svg', event.path);
		}
	});
	gulp.watch('src/svg/**/*.svg', ['svg:base']);
	// png
	gulp.watch('src/images/sprite/*.png', ['sprite']);
});


/*----------------------------------------
	RSYNC DEPLOY
----------------------------------------*/

gulp.task('deploy', function() {
	return gulp.src('build/**')
		.pipe(rsync({
			chmod: "ugo=rwX",
			root: 'build/',
			hostname: 'srv118072@ssh-118072.srv.hoster.ru',
			destination: 'gp-studio.ru/html/spartak/',
			archive: true,
			silent:false,
			compress: true
		}));
});


/*----------------------------------------
	ZIP FILES
----------------------------------------*/
lazyRequireTask('zip', './gulp/tasks/zip', {
	src: paths.allDev,
	dest: paths.tempDir
});

/*----------------------------------------
	FTP
----------------------------------------*/
lazyRequireTask('ftp', './gulp/tasks/ftp', {
	src: paths.allDev
});



gulp.task('default', ['js','svg','server','watch']);

gulp.task('build', ['copy','js','svg','data', 'pug', 'sass', 'svg:base', 'image:min', 'uploads:min']);
